int x , y, w, h;
int speedX, speedY;
int paddleXL, paddleYL, paddleW, paddleH, paddleS;
boolean upL, downL;
 
int paddleXR, paddleYR;
boolean upR, downR;

color colourL = color(5,252,201);
color colourR = color(165,5,525);

int scoreL = 0;
int scoreR = 0;
int winningScore = 5;

import processing.sound.*;
SoundFile ballBounce;
SoundFile song;
SoundFile Gameover;
float volume;
float startTime;
float numSeconds;
boolean isPlaying;

void setup() {
 size(800,500);
  background(0);
  x = width/2;
  y=height/2;
  w=50;
  h= 50;
  speedX = 6;
  speedY = 7;
  
  textSize(40);
  textAlign(CENTER,CENTER);
  
 rectMode(CENTER );
 
 paddleXL = 40;
 paddleYL= height/2;
 
 paddleXR = width - 40;
 paddleYR= height/2;
 
 paddleW= 30;
 paddleH=100;
 paddleS=10;
 
 ballBounce = new SoundFile(this,"ballBounce.wav");
 song = new SoundFile(this,"song.mp3");
 Gameover = new SoundFile(this,"Gameover.mp3");
 //song.loop();
 
 
}

void draw() {
background(0);

 
table();
 
  drawCircle();
  moveCircle();
  bounceOffWalls();
   
   
  drawPaddles();
  movePaddles();
  restrictPaddles();
  contactPaddles();
 
  
  scores();
  gameOver();
  
}
  
  void drawCircle()
  {
  fill(225,0,0);
  ellipse(x, y, w , h);
  }
  
void moveCircle(){
  x=x+speedX;
  y=y+speedY;  }
  
void bounceOffWalls() {
 if ( x > width - w/2) {
   setup();
   speedX = - speedX;
   scoreL = scoreL + 1;}
 else if ( x < 0  + w/2) {
    setup();
    speedX = - speedX;
    scoreR = scoreR+1;}
  if ( y > height - h/2){
    speedY = -speedY; }
  else  if ( y < 0  + h/2) {
    speedY = - speedY;}
     
}
 void drawPaddles(){
 //draw left paddle  
  fill(colourL);
  rect(paddleXL, paddleYL, paddleW, paddleH);
 //draw right paddle  
   fill(colourR);
   rect(paddleXR, paddleYR, paddleW, paddleH);
 }

  void movePaddles () {
//move left paddle
    if ( upL) {
      paddleYL = paddleYL - paddleS;}
    if (downL) {
      paddleYL = paddleYL + paddleS;} 
// move right paddle    
    if ( upR) {
      paddleYR = paddleYR - paddleS;}
    if (downR) {
      paddleYR = paddleYR + paddleS;}
  }

void restrictPaddles(){
 //restrict left paddle
  if (paddleYL - paddleH/2 < 0){
    paddleYL = paddleYL+paddleS;}
  if (paddleYL + paddleH/2 > height){
    paddleYL = paddleYL - paddleS;}
 
 //restrict right paddle 
  if (paddleYR - paddleH/2 < 0){
    paddleYR = paddleYR+paddleS;}
  if (paddleYR + paddleH/2 > height){
    paddleYR = paddleYR - paddleS;}
}

void contactPaddles(){
  //left collision
  if (x - w/2 < paddleXL + paddleW/2 && y - h/2 < paddleYL + paddleH/2 && y + h/2 > paddleYL - paddleH/2 ) {
     if (speedX < 0) {
  speedX = - speedX;
ballBounce.play();
volume= + 100;}
  }

  
  // right collision
 else if (x + w/2 > paddleXR - paddleW/2 && y - h/2 < paddleYR + paddleH/2 && y + h/2 > paddleYR - paddleH/2 ) {
 if (speedX>0){ 
 speedX = - speedX;
 ballBounce.play();
 volume = + 100;} 
 }
}
 
  void scores(){
    fill(colourL);
    text(scoreL,100,50);
    fill(colourR);
    text(scoreR,width-100,50);}

void gameOver(){
  fill(142,137,137);
  
  
 if (scoreL == winningScore) {
   gameOverPage(" Blue Wins!", colourL);

 }
 else if(scoreR == winningScore){
   gameOverPage("Purple Wins!",colourR); 
   
 }
 

}
   void gameOverPage(String text, color c){

     speedX = 0;
     speedY = 0;
     paddleXL = 40;
     paddleYL= height/2;
     paddleXR = width - 40;
     paddleYR= height/2;
      

     textSize(50);
     fill(2555,0,0);
     text ( "Game Over", width/2 -10, height/3-50 - 20);
     textSize(40);
     fill(255);
     text ("Click to Restart Game", width/2 +2, height/3+25, + 50);
     fill(c);
     text (text, width/2 - 10, height/3 - 20);
   
     if (mousePressed) {
       scoreL = 0;
       scoreR = 0;
       speedX = 6;
       speedY = 7;
       
 }
 
   }
   
   void table(){
     background(0);
     stroke(255);
     strokeWeight(4);
     line(width/2,0,width/2,height);
   }
  
  
 
void keyPressed(){
// left paddle controls
if (key == 'w' || key == 'W'){
  upL = true;}
if (key == 's' || key == 'S'){
  downL = true;}
  
// right paddle controls 
if (keyCode == UP){
  upR = true;}
if (keyCode == DOWN){
  downR = true;}
}

void keyReleased(){
// left paddle controls
  if (key == 'w' || key == 'W'){
  upL = false;}
if (key == 's' || key == 'S'){
  downL = false;}
  
//right paddle controls
if (keyCode == UP){
  upR = false;}
if (keyCode == DOWN){
  downR = false;}
}