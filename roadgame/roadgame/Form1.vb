﻿Public Class form1
    Dim Forward_foot As String = "Right"
    Dim xpos, ypos As Integer
    Dim collision As Integer = False
    

    Sub detect_collision()
        Me.TextBox1.Text = collision

        If picsprite.Bounds.IntersectsWith(car1.Bounds) Or
           picsprite.Bounds.IntersectsWith(car2.Bounds) Or
           picsprite.Bounds.IntersectsWith(car3.Bounds) Or
           picsprite.Bounds.IntersectsWith(car4.Bounds) Or
           picsprite.Bounds.IntersectsWith(car5.Bounds) Or
           picsprite.Bounds.IntersectsWith(car6.Bounds) Or
           picsprite.Bounds.IntersectsWith(car7.Bounds) Or
           picsprite.Bounds.IntersectsWith(car8.Bounds) Or
           picsprite.Bounds.IntersectsWith(car9.Bounds) Or
           picsprite.Bounds.IntersectsWith(car10.Bounds) Or
           picsprite.Bounds.IntersectsWith(car11.Bounds) Or
           picsprite.Bounds.IntersectsWith(car12.Bounds) Then
            MessageBox.Show("You died!  \(T_T)/ ")
            xpos = 0
            ypos = 0
            collision = True
        Else

            collision = False

        End If

        If picsprite.Bounds.IntersectsWith(ash.Bounds) Then
            MessageBox.Show("You did it!       \(^-^)/")
            Me.Close()

        End If

    End Sub

    Sub move_sprite_right()

        If Forward_foot = "Right" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            xpos = xpos + 8
            Me.picsprite.Location = New Point(xpos, ypos)
            Me.picsprite.Image = Image.FromFile("pikachu_left.png")
            Forward_foot = "Left"
        ElseIf Forward_foot = "Left" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            xpos = xpos + 8
            Me.picsprite.Image = Image.FromFile("pikachu_right.png")
            Me.picsprite.Location = New Point(xpos, ypos)
            Forward_foot = "Right"
        End If

        If xpos > 1403 Then
            xpos = 1403
        End If

        detect_collision()

        If collision = True Then

            Me.picsprite.Location = New Point(0, 0)

        End If
    End Sub
    Sub move_sprite_left()

        If Forward_foot = "Right" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            xpos = xpos - 8
            Me.picsprite.Location = New Point(xpos, ypos)
            Me.picsprite.Image = Image.FromFile("pikachu_left.png")
            Forward_foot = "Left"
        ElseIf Forward_foot = "Left" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            xpos = xpos - 8
            Me.picsprite.Image = Image.FromFile("pikachu_right.png")
            Me.picsprite.Location = New Point(xpos, ypos)
            Forward_foot = "Right"
        End If
        If xpos < -1 Then
            xpos = -1
        End If
        detect_collision()
        If collision = True Then
            collision = False

            Me.picsprite.Location = New Point(0, 0)

        End If
    End Sub
    Sub move_sprite_down()

        If Forward_foot = "Right" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            ypos = ypos + 8
            Me.picsprite.Location = New Point(xpos, ypos)
            Me.picsprite.Image = Image.FromFile("pikachu_left.png")
            Forward_foot = "Left"
        ElseIf Forward_foot = "Left" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            ypos = ypos + 8
            Me.picsprite.Image = Image.FromFile("pikachu_right.png")
            Me.picsprite.Location = New Point(xpos, ypos)
            Forward_foot = "Right"
        End If

 


        detect_collision()
        If collision = True Then

            Me.picsprite.Location = New Point(0, 0)
        End If
    End Sub
    Sub move_sprite_up()

        If Forward_foot = "Right" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            ypos = ypos - 8
            Me.picsprite.Location = New Point(xpos, ypos)
            Me.picsprite.Image = Image.FromFile("pikachu_left.png")
            Forward_foot = "Left"
        ElseIf Forward_foot = "Left" Then
            Me.picsprite.Location = New Point(xpos, ypos)
            ypos = ypos - 8
            Me.picsprite.Image = Image.FromFile("pikachu_right.png")
            Me.picsprite.Location = New Point(xpos, ypos)
            Forward_foot = "Right"
        End If

        If ypos < 2 Then
            ypos = 2
        End If
        detect_collision()
        If collision = True Then

            Me.picsprite.Location = New Point(0, 0)
        End If
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Left Then
            move_sprite_left()
        End If

        If e.KeyCode = Keys.Right Then
            move_sprite_right()
        End If
        If e.KeyCode = Keys.Down Then
            move_sprite_down()
        End If
        If e.KeyCode = Keys.Up Then
            move_sprite_up()
        End If

    End Sub
    Function randomnumber(ByVal min As Integer, ByRef max As Integer) As Integer

        Dim random As New Random

        Return random.Next(min, max)

    End Function

    Private Sub Move_car1()
        Dim car1__x, car1_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer


        Me.car1.Image = Image.FromFile("carwhite.png")

        car1__x = Me.car1.Left
        car1_y = Me.car1.Top

        For number = 1 To 10
            'move 50 pixels at a time
            Me.car1.Location = New Point(car1__x - 50, car1_y)
            new_x_pos = Me.car1.Left
            new_y_pos = Me.car1.Top
        Next

        random_y = randomnumber(0, 550)
        If car1__x < -5 Then
            Me.car1.Location = New Point(1357, 139)
        End If


    End Sub
    Private Sub Move_car2()

        Dim car2__x, car2_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer


        Me.car2.Image = Image.FromFile("CarBlue.png")

        car2__x = Me.car2.Left
        car2_y = Me.car2.Top


        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car2.Location = New Point(car2__x - 50, car2_y)
        new_x_pos = Me.car2.Left
        new_y_pos = Me.car2.Top

        random_y = randomnumber(0, 550)
        If car2__x < -5 Then
            Me.car2.Location = New Point(1371, 139)
        End If

    End Sub
    Private Sub move_car3()
        Dim car3__x, car3_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer


        Me.car3.Image = Image.FromFile("carsilver.png")

        car3__x = Me.car3.Left
        car3_y = Me.car3.Top


        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car3.Location = New Point(car3__x - 50, car3_y)
        new_x_pos = Me.car3.Left
        new_y_pos = Me.car3.Top

        random_y = randomnumber(0, 550)
        If car3__x < -5 Then
            Me.car3.Location = New Point(1371, 139)
        End If

    End Sub
    Private Sub move_car4()
        Dim car4__x, car4_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car4.Image = Image.FromFile("carwhiterotated.png")

        car4__x = Me.car4.Left
        car4_y = Me.car4.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car4.Location = New Point(car4__x + 50, car4_y)

        new_x_pos = Me.car4.Left
        new_y_pos = Me.car4.Top

        random_y = randomnumber(0, 550)
        If car4__x > +1394 Then
            Me.car4.Location = New Point(-101, 281)
        End If

    End Sub
    Private Sub move_car5()
        Dim car5__x, car5_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car5.Image = Image.FromFile("CarRedRotated.png")

        car5__x = Me.car5.Left
        car5_y = Me.car5.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car5.Location = New Point(car5__x + 50, car5_y)

        new_x_pos = Me.car5.Left
        new_y_pos = Me.car5.Top

        random_y = randomnumber(0, 550)
        If car5__x > +1394 Then
            Me.car5.Location = New Point(-101, 281)
        End If

    End Sub
    Private Sub move_car6()
        Dim car6__x, car6_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car6.Image = Image.FromFile("CarSilverRotated.png")

        car6__x = Me.car6.Left
        car6_y = Me.car6.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car6.Location = New Point(car6__x + 50, car6_y)

        new_x_pos = Me.car6.Left
        new_y_pos = Me.car6.Top

        random_y = randomnumber(0, 550)
        If car6__x > +1394 Then
            Me.car6.Location = New Point(-101, 281)
        End If

    End Sub
    Private Sub move_car8()
        Dim car8__x, car8_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car8.Image = Image.FromFile("CarBlack.png")

        car8__x = Me.car8.Left
        car8_y = Me.car8.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car8.Location = New Point(car8__x - 50, car8_y)

        new_x_pos = Me.car8.Left
        new_y_pos = Me.car8.Top

        random_y = randomnumber(0, 550)
        If car8__x < -5 Then
            Me.car8.Location = New Point(1341, 397)
        End If

    End Sub
    Private Sub move_car7()
        Dim car7__x, car7_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car7.Image = Image.FromFile("carsilver.png")

        car7__x = Me.car7.Left
        car7_y = Me.car7.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car7.Location = New Point(car7__x - 50, car7_y)

        new_x_pos = Me.car7.Left
        new_y_pos = Me.car7.Top

        random_y = randomnumber(0, 550)
        If car7__x < -5 Then
            Me.car7.Location = New Point(1341, 397)
        End If

    End Sub
    Private Sub move_car9()
        Dim car9__x, car9_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car9.Image = Image.FromFile("CarBlue.png")

        car9__x = Me.car9.Left
        car9_y = Me.car9.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car9.Location = New Point(car9__x - 50, car9_y)

        new_x_pos = Me.car9.Left
        new_y_pos = Me.car9.Top

        random_y = randomnumber(0, 550)
        If car9__x < -5 Then
            Me.car9.Location = New Point(1341, 397)
        End If

    End Sub
    Private Sub move_car11()
        Dim car11__x, car11_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car11.Image = Image.FromFile("CarBluerotated.png")

        car11__x = Me.car11.Left
        car11_y = Me.car11.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car11.Location = New Point(car11__x + 50, car11_y)

        new_x_pos = Me.car11.Left
        new_y_pos = Me.car11.Top

        random_y = randomnumber(0, 550)
        If car11__x > +1353 Then
            Me.car11.Location = New Point(-115, 532)
        End If

    End Sub
    Private Sub move_car10()
        Dim car10__x, car10_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car10.Image = Image.FromFile("Car_ Black_rotated.png")

        car10__x = Me.car10.Left
        car10_y = Me.car10.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car10.Location = New Point(car10__x + 50, car10_y)

        new_x_pos = Me.car10.Left
        new_y_pos = Me.car10.Top

        random_y = randomnumber(0, 550)
        If car10__x > +1353 Then
            Me.car10.Location = New Point(-115, 532)
        End If

    End Sub
    Private Sub move_car12()
        Dim car12__x, car12_y As Integer
        Dim number As Integer = 0
        Dim new_x_pos As Integer
        Dim new_y_pos As Integer
        Dim random_y As Integer

        Me.car12.Image = Image.FromFile("CarRedRotated.png")

        car12__x = Me.car12.Left
        car12_y = Me.car12.Top

        For number = 1 To 10

        Next
        'move 50 pixels at a time
        Me.car12.Location = New Point(car12__x + 50, car12_y)

        new_x_pos = Me.car12.Left
        new_y_pos = Me.car12.Top

        random_y = randomnumber(0, 550)
        If car12__x > +1353 Then
            Me.car12.Location = New Point(-115, 532)
        End If

    End Sub
    Private Sub car1timer_Tick(sender As Object, e As EventArgs) Handles car1timer.Tick
        Move_car1()

    End Sub

    Private Sub car2Timer_Tick(sender As Object, e As EventArgs) Handles car2Timer.Tick
        Move_car2()

    End Sub

    Private Sub car3timer_Tick(sender As Object, e As EventArgs) Handles car3timer.Tick
        move_car3()

    End Sub

    Private Sub car4timer_Tick_1(sender As Object, e As EventArgs) Handles car4timer.Tick
        move_car4()
    End Sub

    Private Sub car5timer_Tick(sender As Object, e As EventArgs) Handles car5timer.Tick
        Move_car5()
    End Sub

    
    Private Sub car6timer_Tick(sender As Object, e As EventArgs) Handles car6timer.Tick
        move_car6()
    End Sub

    Private Sub car8timer_Tick(sender As Object, e As EventArgs) Handles car8timer.Tick
        move_car8()
    End Sub

    Private Sub car7timer_Tick(sender As Object, e As EventArgs) Handles car7timer.Tick
        move_car7()
    End Sub

    Private Sub car9timer_Tick(sender As Object, e As EventArgs) Handles car9timer.Tick
        move_car9()
    End Sub

    Private Sub car11timer_Tick(sender As Object, e As EventArgs) Handles car11timer.Tick
        move_car11()
    End Sub

    Private Sub car10timer_Tick(sender As Object, e As EventArgs) Handles car10timer.Tick
        move_car10()
    End Sub

    Private Sub car12timer_Tick(sender As Object, e As EventArgs) Handles car12timer.Tick
        move_car12()
    End Sub


    Private Sub form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class










