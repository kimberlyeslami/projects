﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form1))
        Me.picsprite = New System.Windows.Forms.PictureBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.car1timer = New System.Windows.Forms.Timer(Me.components)
        Me.car2Timer = New System.Windows.Forms.Timer(Me.components)
        Me.car3timer = New System.Windows.Forms.Timer(Me.components)
        Me.car5timer = New System.Windows.Forms.Timer(Me.components)
        Me.car4timer = New System.Windows.Forms.Timer(Me.components)
        Me.car6timer = New System.Windows.Forms.Timer(Me.components)
        Me.car8timer = New System.Windows.Forms.Timer(Me.components)
        Me.car7timer = New System.Windows.Forms.Timer(Me.components)
        Me.car9timer = New System.Windows.Forms.Timer(Me.components)
        Me.car11timer = New System.Windows.Forms.Timer(Me.components)
        Me.car10timer = New System.Windows.Forms.Timer(Me.components)
        Me.car12timer = New System.Windows.Forms.Timer(Me.components)
        Me.ash = New System.Windows.Forms.PictureBox()
        Me.car12 = New System.Windows.Forms.PictureBox()
        Me.car2 = New System.Windows.Forms.PictureBox()
        Me.car3 = New System.Windows.Forms.PictureBox()
        Me.car4 = New System.Windows.Forms.PictureBox()
        Me.car5 = New System.Windows.Forms.PictureBox()
        Me.car6 = New System.Windows.Forms.PictureBox()
        Me.car8 = New System.Windows.Forms.PictureBox()
        Me.car7 = New System.Windows.Forms.PictureBox()
        Me.car9 = New System.Windows.Forms.PictureBox()
        Me.car11 = New System.Windows.Forms.PictureBox()
        Me.car10 = New System.Windows.Forms.PictureBox()
        Me.car1 = New System.Windows.Forms.PictureBox()
        CType(Me.picsprite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ash, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.car1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picsprite
        '
        Me.picsprite.BackColor = System.Drawing.Color.Transparent
        Me.picsprite.Image = CType(resources.GetObject("picsprite.Image"), System.Drawing.Image)
        Me.picsprite.Location = New System.Drawing.Point(-1, 2)
        Me.picsprite.Name = "picsprite"
        Me.picsprite.Size = New System.Drawing.Size(111, 100)
        Me.picsprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picsprite.TabIndex = 25
        Me.picsprite.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(1380, 23)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 26
        '
        'car1timer
        '
        Me.car1timer.Enabled = True
        Me.car1timer.Interval = 40
        '
        'car2Timer
        '
        Me.car2Timer.Enabled = True
        Me.car2Timer.Interval = 40
        '
        'car3timer
        '
        Me.car3timer.Enabled = True
        Me.car3timer.Interval = 40
        '
        'car5timer
        '
        Me.car5timer.Enabled = True
        Me.car5timer.Interval = 40
        '
        'car4timer
        '
        Me.car4timer.Enabled = True
        Me.car4timer.Interval = 40
        '
        'car6timer
        '
        Me.car6timer.Enabled = True
        Me.car6timer.Interval = 40
        '
        'car8timer
        '
        Me.car8timer.Enabled = True
        Me.car8timer.Interval = 40
        '
        'car7timer
        '
        Me.car7timer.Enabled = True
        Me.car7timer.Interval = 40
        '
        'car9timer
        '
        Me.car9timer.Enabled = True
        Me.car9timer.Interval = 40
        '
        'car11timer
        '
        Me.car11timer.Enabled = True
        Me.car11timer.Interval = 40
        '
        'car10timer
        '
        Me.car10timer.Enabled = True
        Me.car10timer.Interval = 40
        '
        'car12timer
        '
        Me.car12timer.Enabled = True
        Me.car12timer.Interval = 40
        '
        'ash
        '
        Me.ash.BackColor = System.Drawing.Color.Transparent
        Me.ash.Image = CType(resources.GetObject("ash.Image"), System.Drawing.Image)
        Me.ash.Location = New System.Drawing.Point(662, 675)
        Me.ash.Name = "ash"
        Me.ash.Size = New System.Drawing.Size(85, 102)
        Me.ash.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ash.TabIndex = 27
        Me.ash.TabStop = False
        '
        'car12
        '
        Me.car12.BackColor = System.Drawing.Color.Transparent
        Me.car12.Image = CType(resources.GetObject("car12.Image"), System.Drawing.Image)
        Me.car12.Location = New System.Drawing.Point(1033, 532)
        Me.car12.Name = "car12"
        Me.car12.Size = New System.Drawing.Size(251, 100)
        Me.car12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car12.TabIndex = 13
        Me.car12.TabStop = False
        '
        'car2
        '
        Me.car2.BackColor = System.Drawing.Color.Transparent
        Me.car2.Image = CType(resources.GetObject("car2.Image"), System.Drawing.Image)
        Me.car2.Location = New System.Drawing.Point(561, 139)
        Me.car2.Name = "car2"
        Me.car2.Size = New System.Drawing.Size(260, 97)
        Me.car2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car2.TabIndex = 9
        Me.car2.TabStop = False
        '
        'car3
        '
        Me.car3.BackColor = System.Drawing.Color.Transparent
        Me.car3.Image = CType(resources.GetObject("car3.Image"), System.Drawing.Image)
        Me.car3.Location = New System.Drawing.Point(1033, 139)
        Me.car3.Name = "car3"
        Me.car3.Size = New System.Drawing.Size(260, 97)
        Me.car3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car3.TabIndex = 5
        Me.car3.TabStop = False
        '
        'car4
        '
        Me.car4.BackColor = System.Drawing.Color.Transparent
        Me.car4.Image = CType(resources.GetObject("car4.Image"), System.Drawing.Image)
        Me.car4.Location = New System.Drawing.Point(85, 270)
        Me.car4.Name = "car4"
        Me.car4.Size = New System.Drawing.Size(274, 100)
        Me.car4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car4.TabIndex = 19
        Me.car4.TabStop = False
        '
        'car5
        '
        Me.car5.BackColor = System.Drawing.Color.Transparent
        Me.car5.Image = CType(resources.GetObject("car5.Image"), System.Drawing.Image)
        Me.car5.Location = New System.Drawing.Point(561, 281)
        Me.car5.Name = "car5"
        Me.car5.Size = New System.Drawing.Size(274, 89)
        Me.car5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car5.TabIndex = 15
        Me.car5.TabStop = False
        '
        'car6
        '
        Me.car6.BackColor = System.Drawing.Color.Transparent
        Me.car6.Image = CType(resources.GetObject("car6.Image"), System.Drawing.Image)
        Me.car6.Location = New System.Drawing.Point(1033, 270)
        Me.car6.Name = "car6"
        Me.car6.Size = New System.Drawing.Size(260, 100)
        Me.car6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car6.TabIndex = 23
        Me.car6.TabStop = False
        '
        'car8
        '
        Me.car8.BackColor = System.Drawing.Color.Transparent
        Me.car8.Image = CType(resources.GetObject("car8.Image"), System.Drawing.Image)
        Me.car8.Location = New System.Drawing.Point(85, 397)
        Me.car8.Name = "car8"
        Me.car8.Size = New System.Drawing.Size(274, 103)
        Me.car8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car8.TabIndex = 7
        Me.car8.TabStop = False
        '
        'car7
        '
        Me.car7.BackColor = System.Drawing.Color.Transparent
        Me.car7.Image = CType(resources.GetObject("car7.Image"), System.Drawing.Image)
        Me.car7.Location = New System.Drawing.Point(561, 397)
        Me.car7.Name = "car7"
        Me.car7.Size = New System.Drawing.Size(274, 103)
        Me.car7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car7.TabIndex = 11
        Me.car7.TabStop = False
        '
        'car9
        '
        Me.car9.BackColor = System.Drawing.Color.Transparent
        Me.car9.Image = CType(resources.GetObject("car9.Image"), System.Drawing.Image)
        Me.car9.Location = New System.Drawing.Point(1033, 406)
        Me.car9.Name = "car9"
        Me.car9.Size = New System.Drawing.Size(251, 94)
        Me.car9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car9.TabIndex = 1
        Me.car9.TabStop = False
        '
        'car11
        '
        Me.car11.BackColor = System.Drawing.Color.Transparent
        Me.car11.Image = CType(resources.GetObject("car11.Image"), System.Drawing.Image)
        Me.car11.Location = New System.Drawing.Point(75, 532)
        Me.car11.Name = "car11"
        Me.car11.Size = New System.Drawing.Size(274, 100)
        Me.car11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car11.TabIndex = 24
        Me.car11.TabStop = False
        '
        'car10
        '
        Me.car10.BackColor = System.Drawing.Color.Transparent
        Me.car10.Image = CType(resources.GetObject("car10.Image"), System.Drawing.Image)
        Me.car10.Location = New System.Drawing.Point(561, 532)
        Me.car10.Name = "car10"
        Me.car10.Size = New System.Drawing.Size(274, 100)
        Me.car10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car10.TabIndex = 21
        Me.car10.TabStop = False
        '
        'car1
        '
        Me.car1.BackColor = System.Drawing.Color.Transparent
        Me.car1.Image = CType(resources.GetObject("car1.Image"), System.Drawing.Image)
        Me.car1.Location = New System.Drawing.Point(85, 139)
        Me.car1.Name = "car1"
        Me.car1.Size = New System.Drawing.Size(274, 97)
        Me.car1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.car1.TabIndex = 3
        Me.car1.TabStop = False
        '
        'form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1526, 789)
        Me.Controls.Add(Me.car12)
        Me.Controls.Add(Me.car10)
        Me.Controls.Add(Me.car11)
        Me.Controls.Add(Me.car9)
        Me.Controls.Add(Me.car7)
        Me.Controls.Add(Me.car8)
        Me.Controls.Add(Me.car6)
        Me.Controls.Add(Me.car5)
        Me.Controls.Add(Me.car4)
        Me.Controls.Add(Me.car3)
        Me.Controls.Add(Me.car2)
        Me.Controls.Add(Me.car1)
        Me.Controls.Add(Me.ash)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.picsprite)
        Me.Name = "form1"
        Me.Text = "  "
        CType(Me.picsprite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ash, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.car1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picsprite As System.Windows.Forms.PictureBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents car1timer As System.Windows.Forms.Timer
    Friend WithEvents car2Timer As System.Windows.Forms.Timer
    Friend WithEvents car3timer As System.Windows.Forms.Timer
    Friend WithEvents car5timer As System.Windows.Forms.Timer
    Friend WithEvents car4timer As System.Windows.Forms.Timer
    Friend WithEvents car6timer As System.Windows.Forms.Timer
    Friend WithEvents car8timer As System.Windows.Forms.Timer
    Friend WithEvents car7timer As System.Windows.Forms.Timer
    Friend WithEvents car9timer As System.Windows.Forms.Timer
    Friend WithEvents car11timer As System.Windows.Forms.Timer
    Friend WithEvents car10timer As System.Windows.Forms.Timer
    Friend WithEvents car12timer As System.Windows.Forms.Timer
    Friend WithEvents ash As System.Windows.Forms.PictureBox
    Friend WithEvents car12 As System.Windows.Forms.PictureBox
    Friend WithEvents car2 As System.Windows.Forms.PictureBox
    Friend WithEvents car3 As System.Windows.Forms.PictureBox
    Friend WithEvents car4 As System.Windows.Forms.PictureBox
    Friend WithEvents car5 As System.Windows.Forms.PictureBox
    Friend WithEvents car6 As System.Windows.Forms.PictureBox
    Friend WithEvents car8 As System.Windows.Forms.PictureBox
    Friend WithEvents car7 As System.Windows.Forms.PictureBox
    Friend WithEvents car9 As System.Windows.Forms.PictureBox
    Friend WithEvents car11 As System.Windows.Forms.PictureBox
    Friend WithEvents car10 As System.Windows.Forms.PictureBox
    Friend WithEvents car1 As System.Windows.Forms.PictureBox

End Class
